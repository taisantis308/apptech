-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 06-Out-2019 às 22:43
-- Versão do servidor: 10.3.16-MariaDB
-- versão do PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `apptech`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `adms_cors`
--

CREATE TABLE `adms_cors` (
  `id` int(11) NOT NULL,
  `nome` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `cor` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `adms_cors`
--

INSERT INTO `adms_cors` (`id`, `nome`, `cor`, `created`, `modified`) VALUES
(1, 'Azul', 'primary', '2018-05-23 00:00:00', '2018-06-29 20:06:34'),
(2, 'Cinza', 'secondary', '2018-05-23 00:00:00', NULL),
(3, 'Verde', 'success', '2018-05-23 00:00:00', NULL),
(4, 'Vermelho', 'danger', '2018-05-23 00:00:00', NULL),
(5, 'Laranjado', 'warning', '2018-05-23 00:00:00', NULL),
(6, 'Azul claro', 'info', '2018-05-23 00:00:00', NULL),
(7, 'Claro', 'light', '2018-05-23 00:00:00', NULL),
(8, 'Cinza escuro', 'dark', '2018-05-23 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `adms_robots`
--

CREATE TABLE `adms_robots` (
  `id` int(11) NOT NULL,
  `nome` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `adms_robots`
--

INSERT INTO `adms_robots` (`id`, `nome`, `tipo`, `created`, `modified`) VALUES
(1, 'Indexar a pagina e seguir os links', 'index,follow', '2019-09-24 00:00:00', NULL),
(2, 'Nao indexar a pagina mas seguir os links', 'noindex,follow', '2019-09-24 00:00:00', NULL),
(3, 'Indexar a pagina mas nao seguir os links', 'index,nofollow', '2019-09-24 00:00:00', NULL),
(4, 'Nao indexar a pagina e nem seguir os links', 'noindex,nofollow', '2019-09-24 00:00:00', NULL),
(5, 'Nao exibir a versao em cache da pagina', 'noarchive', '2019-09-24 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sts_artigos`
--

CREATE TABLE `sts_artigos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `conteudo` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `resumo_publico` text COLLATE utf8_unicode_ci NOT NULL,
  `qnt_acesso` int(11) NOT NULL DEFAULT 0,
  `sts_robot_id` int(11) NOT NULL,
  `adms_usuario_id` int(11) NOT NULL,
  `adms_sit_id` int(11) NOT NULL,
  `sts_tps_artigo_id` int(11) NOT NULL,
  `sts_cats_artigo_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sts_artigos`
--

INSERT INTO `sts_artigos` (`id`, `titulo`, `descricao`, `conteudo`, `imagem`, `slug`, `keywords`, `description`, `author`, `resumo_publico`, `qnt_acesso`, `sts_robot_id`, `adms_usuario_id`, `adms_sit_id`, `sts_tps_artigo_id`, `sts_cats_artigo_id`, `created`, `modified`) VALUES
(1, 'Impacto da Tecnologia nos negocios', 'Com o crescimento da tecnologias, que sofreu um salto significativo nos ultimos 20 anos, torna-se cada vez mais importante entender como isso afeta nossos empreendimentos.', '<p>Nos ultimos 20 anos a tecnologia sofreu um crescimento demasiado, de modo que as empresas precisaram adapta-las ao seu contexto, independente de autarem ou nao no seto tecnologico.</p>\r\n								<hr>\r\n								<p>O fenomeno da Internet faz parte de um processo de gerenciamento das novas proposicoes. Percebemos, cada vez mais, que o surgimento do comercio virtual maximiza as possibilidades por conta do retorno esperado a longo prazo. No mundo atual, a adocao de politicas descentralizadoras possibilita uma melhor visao global dos paradigmas corporativos. Todavia, a hegemonia do ambiente politico facilita a criação do processo de comunicaçao como um todo.</p>\r\n								\r\n								  <p>Pensando mais a longo prazo, a estrutura atual da organizacao ainda nao demonstrou convincentemente que vai participar na mudanca de todos os recursos funcionais envolvidos. Ainda assim, existem duvidas a respeito de como a mobilidade dos capitais internacionais acarreta um processo de reformulacao e modernizacao do sistema de formacao de quadros que corresponde as necessidades. Importante enfatizar que o entendimento das metas propostas prepara-nos para enfrentar situacoes atipicas decorrentes dos niveis de motivacao departamental. Todavia, o desafiador cenario globalizado facilita a criacao das diversas correntes de pensamento. Neste sentido, o fenomeno da Internet causa impacto indireto na reavaliacao do sistema de participacao geral.</p>\r\n								\r\n								<p> Do mesmo modo, o desenvolvimento continuo de distintas formas de atuacao talvez venha a ressaltar a relatividade do orcamento setorial. E importante questionar o quanto o consenso sobre a necessidade de qualificacao assume importantes posicoes no estabelecimento dos conhecimentos estrategicos para atingir a excelencia. O novo modelo estrutural aqui preconizado agrega valor ao estabelecimento de todos os recursos funcionais envolvidos.</p>\r\n						', 'artigo.jpg', 'sample-blog-post-1', 'artigo, artigo 1, ', 'Descricao do artigo um', 'Tainá', '<p>Com o crescimento da tecnologias, que sofreu um salto significativo nos ultimos 20 anos, torna-se cada vez mais importante entender como isso afeta nossos empreendimentos.</p>\r\n ', 6, 6, 1, 1, 1, 1, '2019-09-28 00:00:00', '2019-09-29 15:38:22'),
(2, 'Tecnologia e o mundo atual', 'Atualmente a tecnologia tem-nos transportado a lugares inimaginaveis. A tecnologia tem-nos permitido, acima de tudo, a construcao de um novo mundo.                ', '<p>\"O ser humano, dotado de sua inteligencia, buscou formas, durante toda a historia da tecnologia de vencer os obstaculos impostos pela natureza. Desta forma, foi desenvolvendo e inventando instrumentos tecnologicos com o objetivo de superar dificuldades. Podemos dizer que a necessidade eh a mae das grandes invencoes tecnologicas\". Fonte: <i>historiasobreossitesdebusca</i></p>\r\n								<hr>\r\n								<p>O fenomeno da Internet faz parte de um processo de gerenciamento das diversas correntes de pensamento. O empenho em analisar a execucao dos pontos do programa eh uma das consequencias do sistema de participacao geral. Assim mesmo, a consulta aos diversos militantes nos obriga a analise dos relacionamentos verticais entre as hierarquias.</p>\r\n								<blockquote>\r\n								  <p> As experiencias acumuladas demonstram que a consolidacao das estruturas estende o alcance e a importancia das condicoes inegavelmente apropriadas. A pratica cotidiana prova que a percepcao das dificuldades auxilia a preparacao e a composicao dos conhecimentos estrategicos para atingir a excelencia. Acima de tudo, eh fundamental ressaltar que a determinacao clara de objetivos oferece uma interessante oportunidade para verificacao das formas de acao. Nao obstante, o acompanhamento das preferencias de consumo pode nos levar a considerar a reestruturacao dos paradigmas corporativos. Todas estas questoes, devidamente ponderadas, levantam duvidas sobre se o novo modelo estrutural aqui preconizado agrega valor ao estabelecimento dos relacionamentos verticais entre as hierarquias.\r\n</p>\r\n								</blockquote>\r\n								\r\n								\r\n								<p> Evidentemente, a crescente influencia da midia possibilita uma melhor visao global das diretrizes de desenvolvimento para o futuro. O que temos que ter sempre em mente eh que o comprometimento entre as equipes deve passar por modificacoes independentemente do levantamento das variaveis envolvidas. Por outro lado, a consulta aos diversos militantes aponta para a melhoria de alternativas as solucoes ortodoxas. Podemos ja vislumbrar o modo pelo qual a complexidade dos estudos efetuados assume importantes posicoes no estabelecimento do impacto na agilidade decisoria.</p>\r\n						                    ', 'artigo.jpg', 'sample-blog-post-2', 'artigo, artigo 2, ', 'Descricao do artigo dois', 'Tainá', '<p>Atualmente a tecnologia tem-nos transportado a lugares inimaginaveis. A tecnologia tem-nos permitido, acima de tudo, a construcao de um novo mundo. </p>', 4, 1, 1, 1, 1, 3, '2019-09-28 00:00:00', '2019-09-29 15:34:08'),
(3, 'Linguagens de programacao mais populares', 'Uma das grandes dificuldades na hora de desenvolver, seja sistemas desktop, seja web, eh escolher quais linguagens de programacao serao utilizadas e quais melhores se adequam as nossas necessidades.', '<p>Atualmente, temos uma ampla gama de tecnologias, embora seja vantajoso podemos escolher entre milhares, pois nao ficamos sem opcoes, isso demanda mais tempo e esfoco</p>\r\n								<hr>\r\n								<p>A linguagem de programacao eh um metodo padronizado para comunicar instrucoes para um computador. Eh um conjunto de regras sintaticas e semanticas usadas para definir um programa de computador. Permite que um programador especifique precisamente sobre quais dados um computador vai atuar, como estes dados serao armazenados ou transmitidos e quais acoes devem ser tomadas sob varias circunstancias. Linguagens de programacao podem ser usadas para expressar algoritmos com precisao. (fonte: <i>Wikipedia<i>)</p>\r\n								<blockquote>\r\n								  <p>Atualmente existem varias linguagens de programacao, cada linguagem possui sua particularidade e podem ser empregadas para um ou varios fins.</p>\r\n								</blockquote>\r\n								\r\n								<h2>20 linguagens de programacao mais populares</h2>\r\n								<p>De acordo com o indice <a href=\"https://www.tiobe.com/tiobe-index/\">TIOBE</a>, as 20 linguagens de programacao mais populares sao: </p>\r\n								\r\n<ol>\r\n									<li>Java</li>\r\n									<li>C</li>\r\n									<li>Python</li>\r\n\r\n<li>C++</li>\r\n\r\n<li>C#</li>\r\n\r\n<li>Visual Basic .NET</li>\r\n\r\n<li>JavaScript</li>\r\n\r\n<li>SQL</li>\r\n\r\n<li>PHP</li>\r\n\r\n<li>Objective-C</li>\r\n\r\n<li>Groovy</li>\r\n\r\n<li>Swift</li>\r\n\r\n<li>Ruby</li>\r\n\r\n<li>Assembly Language</li>\r\n\r\n<li>R</li>\r\n\r\n<li>Visual Basic</li>\r\n\r\n<li>Go</li>\r\n\r\n<li>Delphi</li>\r\n\r\n<li>Pascal</li>\r\n\r\n<li>MATLAB</li>\r\n								</ol>							\r\n\r\n								<p>A lista acima, tem como referencia o mes de outubro de 2019.</p>', 'artigo.jpg', 'sample-blog-post-3', 'artigo, artigo 3 ', 'Descricao do artigo tres', 'Tainá', '<p>Uma das grandes dificuldades na hora de desenvolver, seja sistemas desktop, seja web, eh escolher quais linguagens de programacao serao utilizadas e quais melhores se adequam as nossas necessidades.</p>\r\n                        ', 14, 1, 1, 1, 1, 1, '2019-09-28 00:00:00', NULL),
(4, 'Bitcoin', 'O que eh e como funciona o Bitcoin?                  ', '<p>Bitcoin eh uma criptomoeda descentralizada ou um dinheiro eletronico para transacoes ponto-a-ponto apresentada em 2008, na lista de discussao The Cryptography Mailing por um programador ou grupo de programadores sob o pseudonimo Satoshi Nakamoto, eh considerada a primeira moeda digital mundial descentralizada.</p>\r\n								<hr>\r\n								<p>O Bitcoin permite transacoes financeiras sem intermediarios, mas verificadas por todos usuarios (nodos) da rede, que sao gravadas em um banco de dados distribuidos, chamado de blockchain, uma rede descentralizada, isto eh, uma estrutura sem uma entidade administradora central, o que torna inviavel qualquer autoridade financeira ou governamental manipular a emissao e o valor da criptomoeda ou induzir a inflacao com a producao de mais dinheiro. No entanto, grandes movimentos especulativos de oferta e demanda influenciam na oscilacao de seu valor no mercado de cambio, sendo definido livremente durante as 24 horas do dia. Fonte <i>Wikipedia</i></p>\r\n								\r\n															<h2>Vantagens</h2>\r\n								<p>Nenhum Bitcoin pode ser confiscado, voce pode manda-lo para quem quiser pagando taxas menores. Alem disso, ele e seguro e da mais privacidade, controle e transparencia nas negociacoes. Contudo, o Bitcoin pode ser utilizado tambem como investimento para que esta procurando diversificar seu patrimonio para ter melhores ganhos. O Bitcoin trouxe excelentes retornos e a tendencia eh que ele se torne mais popular a longo prazo. Fonte: <a href=\"https://foxbit.com.br/o-que-e-bitcoin/\">Foxbit</a></p>\r\n						                ', 'artigo.jpg', 'sample-blog-post-4', 'artigo, artigo 4 ', 'Descricao do artigo quatro', 'Tainá', '<p>Bitcoin é uma moeda digital, descentralizada e que não necessita de terceiros para funcionar.</p>\r\n                                             ', 13, 1, 1, 1, 1, 1, '2019-09-28 00:00:00', '2019-09-29 16:17:06'),
(5, 'Utilizacao de midias sociais nos negocios', 'O crescimento na utilizacao de midias sociais podem impactar positivamente nos negocios. ', '<p>O crescimento na utilizacao de midias sociais podem impactar positivamente nos negocios.</p>\r\n								<hr>\r\n								<p>Quando o assunto eh marketing digital, as redes sociais estao entre as ferramentas fundamentais de uma estrategia efetiva. De acordo com dados levantados pela E-marketer, mais de 100 milhoes de brasileiros estao conectados a redes como Facebook, Instagram e Twitter.</p>\r\n								<blockquote>\r\n								  <p>Tal numero apenas comprova a importancia de promover o seu negocio nas redes sociais a partir de conteudos que estimulem interacoes e compartilhamentos. Ao engajar sua empresa nesses canais, eh possivel captar novos clientes e conquistar novos mercados, aumentando nao so o numero de vendas, mas tambem sua autoridade perante a concorrencia. Fonte: <a href=\"https://www.conectandopessoas.com.br/geral/a-importancia-das-redes-sociais-para-o-seu-negocio/\">Conectando pessoas</a></p>\r\n								</blockquote>\r\n								<p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>\r\n								<p>O investimento em midias sociais e marketing digital, traz resultados como grande visibilidade perante o publico, aumento significativo nas vendas, aproximacao com publico-alvo, viralizacao de conteudo, o que faz com que o publico atingido seja cada vez maior.</p>\r\n								<h3>Como a AppTech pode te auxiliar?</h3>\r\n								<p>A AppTech atua com consultoria, de modo amplo, e atende o segmento de marketing digital. Com a AppTech, ha a garantia de trabalhar a imagem digital da sua empresa, treinamento especializado para criacao de postagens e desenvolvimento de ferramentas que aproximem sua empresa do publico. O diferencial da AppTech, eh a busca por tornar o marketing digital mais humanizado, utilizando o linguajar e atendendo as expectativas do publico.</p>\r\n								', 'artigo.jpg', 'sample-blog-post-5', 'artigo, artigo 5', 'Descricao do artigo cinco', 'Tainá', '<p>A AppTech atua com consultoria, de modo amplo, e atende o segmento de marketing digital. Com a AppTech, ha a garantia de trabalhar a imagem digital da sua empresa, treinamento especializado para criacao de postagens e desenvolvimento de ferramentas que aproximem sua empresa do publico. </p>\r\n                          ', 1, 1, 1, 1, 1, 1, '2019-09-28 00:00:00', NULL),
(6, 'O impacto da Seguranca da Informacao nos negocios', '<p>A seguranca da informacao, eh uma area de grande importante para cada empresa. De pequenos a grandes negocios, todos devem pensar no prejuizo que o vazamento de dados pode causar a empresa.</p>', '<p>A seguranca da informacao, eh uma area de grande importante para cada empresa. De pequenos a grandes negocios, todos devem pensar no prejuizo que o vazamento de dados pode causar a empresa.</p>\r\n\r\n<hr>\r\n\r\n<p>Seguranca da Informacao eh a area que envolve um conjunto de medidas necessarias por garantir que a confidencialidade, integridade e disponibilidade das informacoes de uma organizacao ou individuo de forma a preservar esta informacao de acordo com necessidades especificas.</p>\r\n\r\n<p>A area de seguranca da informacao engloba os seguintes pilares:</p>\r\n\r\n<ul>\r\n<li> Confidencialidade: pricipio que requer que os dados sejam acessados somente por pessoas autorizadas</li>\r\n<li>Integridade: principio que garante de que dados sejam modificados por pessoas e atividades autorizadas.</li>\r\n<li>Disponibilidade: principio que garante que a informacao seja acessada por pessoas autorizadas, sempre que for requisitada</li>\r\n\r\n<br><br>\r\n\r\n<h3>Como a AppTech pode te auxiliar com seguranca da informacao nos seus negocios?</h3>\r\n\r\n<p>A AppTech eh excelencia em tecnologia da informacao, fornecendo treinamento especializado para seus colaboradores, de modo que todos saibam a importancia da seguranca da informacao, e como cada um pode cooperar para um ambiente de trabalho mais seguro, tanto para si, quanto para as pessoas ao redor. A AppTech tambem possui uma equipe de desenvolvimento altamente especializada em desenvolvimento de solucoes de seguranca.</p>\r\n\r\n', 'artigo.jpg', 'sample-blog-post-6', 'artigo, artigo 6', 'Descricao do artigo seis para seo', 'Tainá', '<p>A AppTech eh excelencia em tecnologia da informacao, fornecendo treinamento especializado para seus colaboradores, de modo que todos saibam a importancia da seguranca da informacao, e como cada um pode cooperar para um ambiente de trabalho mais seguro, tanto para si, quanto para as pessoas ao redor.</p>', 20, 4, 1, 1, 1, 1, '2019-09-23 00:00:00', '2019-09-29 18:04:08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sts_carousels`
--

CREATE TABLE `sts_carousels` (
  `id` int(11) NOT NULL,
  `nome` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(220) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` varchar(220) COLLATE utf8_unicode_ci DEFAULT NULL,
  `posicao_text` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_botao` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(220) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `adms_cor_id` int(11) DEFAULT NULL,
  `adms_sit_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sts_carousels`
--

INSERT INTO `sts_carousels` (`id`, `nome`, `imagem`, `titulo`, `descricao`, `posicao_text`, `titulo_botao`, `link`, `ordem`, `adms_cor_id`, `adms_sit_id`, `created`, `modified`) VALUES
(1, 'Primeiro Exemplo', 'imagem_um.jpg', 'Conheca a AppTech', 'Entenda nosso perfil e como podemos impactar nos diversos segmentos e negocios de pequenas a grandes empresas.', 'text-left', 'Mais detalhes...', 'http://localhost/apptech/sobre-empresa', 1, 1, 1, '2019-09-28 00:00:00', '2019-09-28 14:22:07'),
(2, 'Segundo Exemplo', 'imagem_dois.jpg', 'Tempo eh dinheiro', 'Entenda como a utilizacao de bitcoin pode impactar o sua empresa', 'text-center', 'Saiba mais...', 'http://localhost/apptech/artigo/sample-blog-post-4', 2, 5, 1, '2019-09-28 00:00:00', '2019-09-28 14:22:07'),
(3, 'Terceiro Exemplo', 'imagem_tres.jpg', 'Tecnologia nos negocios', 'Entenda como a tecnologia pode ter impacto no crescimento dos negocios', 'text-right', 'Saiba mais...', 'http://localhost/apptech/artigo/sample-blog-post-1', 3, 4, 1, '2019-09-28 00:00:00', '2019-09-28 14:22:07');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sts_cats_artigos`
--

CREATE TABLE `sts_cats_artigos` (
  `id` int(11) NOT NULL,
  `nome` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `adms_sit_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sts_cats_artigos`
--

INSERT INTO `sts_cats_artigos` (`id`, `nome`, `adms_sit_id`, `created`, `modified`) VALUES
(1, 'PHP', 1, '2019-09-27 00:00:00', NULL),
(2, 'Bootstrap', 1, '2019-09-27 00:00:00', NULL),
(3, 'MySQLi', 1, '2019-09-27 00:00:00', '2019-09-29 12:29:16'),
(6, 'teste 12', 1, '2019-09-29 16:48:00', '2019-09-29 16:48:08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sts_contatos`
--

CREATE TABLE `sts_contatos` (
  `id` int(11) NOT NULL,
  `nome` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `assunto` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sts_contatos`
--

INSERT INTO `sts_contatos` (`id`, `nome`, `email`, `assunto`, `mensagem`, `created`, `modified`) VALUES
(1, 'Taina', 'taina@apptech.com.br', 'teste1', 'msg teste 1', '2019-09-26 11:33:16', NULL),
(2, 'Taina', 'taina@apptech.com.br', 'teste1', 'msg teste 1', '2019-09-26 11:33:27', NULL),
(3, 'Taina', 'taina@apptech.com.br', 'teste1', 'msg teste 1', '2019-09-27 10:16:36', NULL),
(4, 'Taina', 'taina@apptech.com.br', 'teste1', 'msg teste 1', '2019-09-27 10:32:34', NULL),
(5, 'Taina', 'taina@apptech.com.br', 'teste1', 'msg teste 1', '2019-09-27 10:32:47', NULL),
(6, 'Taina', 'taina@apptech.com.br', 'Teste 6', 'Teste 6', '2019-09-27 11:20:12', NULL),
(7, 'Taina', 'taina@apptech.com.br', 'Teste 6', 'Teste 6', '2019-09-27 11:20:12', NULL),
(8, 'Taina', 'taina@apptech.com.br', 'Teste 8', 'Teste 8', '2019-09-27 11:39:49', NULL),
(9, 'Taina', '', 'Teste 9', 'Teste 9', '2019-09-27 11:40:12', NULL),
(10, 'Taina', 'taina@apptech.com.br', 'Teste 10', 'Teste 10', '2019-09-29 11:10:13', NULL),
(11, 'Taina', 'taina@apptech.com.br', 'Teste 11', 'Teste 11', '2019-09-29 11:10:21', NULL),
(12, 'Taina', 'taina@apptech.com.br', 'Teste 12', 'Teste 12', '2019-09-29 11:16:20', NULL),
(14, 'Taina', 'taisantis308@gmail.com', 'Teste', 'tenjbf', '2019-10-06 20:46:37', NULL),
(15, 'teste', 'tese@gmail.com', 'teste', 'jnjfbdhb', '2019-10-06 21:59:02', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sts_paginas`
--

CREATE TABLE `sts_paginas` (
  `id` int(11) NOT NULL,
  `controller` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `nome_pagina` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `obs` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lib_bloq` int(11) NOT NULL DEFAULT 2,
  `ordem` int(11) NOT NULL,
  `sts_tps_pg_id` int(11) NOT NULL,
  `sts_robot_id` int(11) NOT NULL,
  `sts_situacaos_pg_id` int(11) NOT NULL DEFAULT 2,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sts_paginas`
--

INSERT INTO `sts_paginas` (`id`, `controller`, `endereco`, `nome_pagina`, `titulo`, `obs`, `keywords`, `description`, `author`, `imagem`, `lib_bloq`, `ordem`, `sts_tps_pg_id`, `sts_robot_id`, `sts_situacaos_pg_id`, `created`, `modified`) VALUES
(1, 'Home', 'home', 'Pagina inicial', 'apptech - Pagina inicial', 'Pagina inicial do site do projeto sts', 'noticias,...', 'Site de noticias sobre...', 'apptech', 'home.jpg', 1, 1, 1, 1, 1, '2019-09-24 00:00:00', NULL),
(2, 'SobreEmpresa', 'sobre-empresa', 'Sobre Empresa', 'apptech - Sobre Empresa', 'Pagina sobre empresa do site do projeto sts', 'sobre a empresa apptech, apptech', 'A empresa apptech...', 'apptech', 'sobre_empresa.jpg', 1, 2, 1, 1, 1, '2019-09-24 00:00:00', NULL),
(3, 'Blog', 'blog', 'Blog', 'apptech - Blog', 'Pagina blog do site do projeto sts', 'Ultimas noticias, noticias sobre...', 'Ultimas noticias sobre...', 'apptech', 'blog.jpg', 1, 3, 1, 1, 1, '2019-09-24 00:00:00', NULL),
(4, 'Artigo', 'artigo', 'Artigo', 'apptech = Artigo', 'Pagina para ver o artigo inteiro no site do projeto sts', 'php, php oo,...', 'Como criar o ...', 'apptech', 'artigo.jpg', 2, 4, 1, 1, 1, '2019-09-24 00:00:00', NULL),
(5, 'Contato', 'contato', 'Contato', 'apptech - Contato', 'Pagina contato no site do projeto sts', 'contato, contato com,...', 'Formulario de contato...', 'apptech', 'contato.jpg', 1, 5, 1, 1, 1, '2019-09-24 00:00:00', NULL),
(6, 'Loja', 'loja', 'Loja', 'apptech - Vitrine da Loja', 'Pagina para listas os produtos', 'Pagina para listas os produtos', 'Pagina para listas os produtos', 'apptech', 'loja.jpg', 2, 6, 2, 1, 1, '2019-09-24 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sts_robots`
--

CREATE TABLE `sts_robots` (
  `id` int(11) NOT NULL,
  `nome` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sts_robots`
--

INSERT INTO `sts_robots` (`id`, `nome`, `tipo`, `created`, `modified`) VALUES
(1, 'Indexar a pagina e seguir os links', 'index,follow', '2019-09-24 00:00:00', NULL),
(2, 'Nao indexar a pagina mas seguir os links', 'noindex,follow', '2019-09-24 00:00:00', NULL),
(3, 'Indexar a pagina mas nao seguir os links', 'index,nofollow', '2019-09-24 00:00:00', NULL),
(4, 'Nao indexar a pÃ¡gina e nem seguir os links', 'noindex,nofollow', '2019-09-24 00:00:00', NULL),
(5, 'Nao exibir a versao em cache da pagina', 'noarchive', '2019-09-24 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sts_seo`
--

CREATE TABLE `sts_seo` (
  `id` int(11) NOT NULL,
  `og_site_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `og_locale` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `fb_admins` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `twitter_site` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sts_seo`
--

INSERT INTO `sts_seo` (`id`, `og_site_name`, `og_locale`, `fb_admins`, `twitter_site`, `created`, `modified`) VALUES
(1, 'Apptech', 'pt_BR', '100009416086586', '@apptech', '2019-09-28 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sts_servicos`
--

CREATE TABLE `sts_servicos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `icone_um` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nome_um` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `descricao_um` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `icone_dois` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nome_dois` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `descricao_dois` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `icone_tres` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nome_tres` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `descricao_tres` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sts_servicos`
--

INSERT INTO `sts_servicos` (`id`, `titulo`, `icone_um`, `nome_um`, `descricao_um`, `icone_dois`, `nome_dois`, `descricao_dois`, `icone_tres`, `nome_tres`, `descricao_tres`, `created`, `modified`) VALUES
(1, 'Servicos', 'ion-locked', 'Seguranca da Informacao', 'Servico especializado em seguranca da informacao, implantacao das melhores praticas e ferramentas existentes no mercado, treinamento de funcionarios.', 'ion-at', 'Consultoria digital', 'Consultoria e marketing digital para otimizacao de negocios, divulgacao de negocios nas principais midias sociais.', 'ion-gear-a', 'Gerenciamento de Servicos', 'Consultoria em gestao de servicos de tecnologia.', '2019-09-24 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sts_situacaos_pgs`
--

CREATE TABLE `sts_situacaos_pgs` (
  `id` int(11) NOT NULL,
  `nome` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `adms_cor_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sts_situacaos_pgs`
--

INSERT INTO `sts_situacaos_pgs` (`id`, `nome`, `adms_cor_id`, `created`, `modified`) VALUES
(1, 'Ativo', 3, '2019-09-24 00:00:00', NULL),
(2, 'Inativo', 5, '2019-09-24 00:00:00', NULL),
(3, 'Analise', 1, '2019-09-24 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sts_sobres`
--

CREATE TABLE `sts_sobres` (
  `id` int(11) NOT NULL,
  `titulo` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `adms_sit_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sts_sobres`
--

INSERT INTO `sts_sobres` (`id`, `titulo`, `descricao`, `imagem`, `adms_sit_id`, `created`, `modified`) VALUES
(1, 'Sobre Autor', 'Formado em Analise e Desenvolvimento de Sistemas pelo Instituto Federal de Sao Paulo, fundador da AppTech, presta consultoria desde 2016 para medias e grandes empresas de diversos ramos.', 'apptech.jpg', 1, '2019-09-24 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sts_sobs_emps`
--

CREATE TABLE `sts_sobs_emps` (
  `id` int(11) NOT NULL,
  `titulo` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `adms_sit_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sts_sobs_emps`
--

INSERT INTO `sts_sobs_emps` (`id`, `titulo`, `descricao`, `imagem`, `ordem`, `adms_sit_id`, `created`, `modified`) VALUES
(1, 'Missao', 'Promover as melhores praticas em seguranca tecnologia na melhoria de servicos, alem de atendimento humano e personalizado para nossos clientes ', 'missao.jpg', 1, 1, '2019-09-24 00:00:00', NULL),
(2, 'Visao', 'Ser uma consultoria de tecnologia da informacao reconhecida pelo melhor atendimento e pelas melhores praticas empregadas em cada atendimento prestado.', 'visao.jpg', 2, 1, '2019-09-24 00:00:00', NULL),
(3, 'Valores', 'Nossos valores se resumem a sustentabilidade e humanizacao de atendimento. Acreditamos que trabalhar com sustentabilidade torna nosso mundo ao redor um lugar melhor para se viver, e que tecnologia sem humanizacao eh apenas tecnologia.', 'valor.jpg', 3, 1, '2019-09-24 00:00:00', NULL),
(4, 'Cultura', 'Nossa cultura baseia-se na filosofia de mundo que temos. Acreditamos que um mundo melhor eh onde as pessoas sao acolhedores e se unem em prol de um bem melhor. Na AppTech, buscar integrar tecnologia com espirito de humanidade. Buscamos que os nossos colaboradores se sintam acolhidos para que possam acolher, e estejam sempre motivados a trabalhar em equipe, trazer novas ideias e se desenvolver.', 'cultura.jpg', 4, 1, '2019-09-24 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sts_tps_artigos`
--

CREATE TABLE `sts_tps_artigos` (
  `id` int(11) NOT NULL,
  `nome` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sts_tps_artigos`
--

INSERT INTO `sts_tps_artigos` (`id`, `nome`, `created`, `modified`) VALUES
(1, 'Publico', '2019-09-24 00:00:00', NULL),
(2, 'Privado', '2019-09-24 00:00:00', NULL),
(3, 'Privado com resumo publico', '2019-09-24 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sts_tps_pgs`
--

CREATE TABLE `sts_tps_pgs` (
  `id` int(11) NOT NULL,
  `tipo` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `obs` text COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sts_tps_pgs`
--

INSERT INTO `sts_tps_pgs` (`id`, `tipo`, `nome`, `obs`, `ordem`, `created`, `modified`) VALUES
(1, 'sts', 'Site Principal', 'Core do site principal', 1, '2019-09-24 00:00:00', NULL),
(2, 'lojas', 'Loja', 'Loja virtual', 2, '2019-09-24 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sts_videos`
--

CREATE TABLE `sts_videos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(220) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sts_videos`
--

INSERT INTO `sts_videos` (`id`, `titulo`, `descricao`, `video`, `created`, `modified`) VALUES
(1, 'Video', '8 tecnologias inventadas por brasileiros. By: Tecnmundo', ' <iframe class=\"embed-responsive-item\" src=\"https://www.youtube.com/embed/Yni_QlE8y5E?rel=0\" allowfullscreen></iframe>', '2019-09-24 00:00:00', NULL);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `adms_robots`
--
ALTER TABLE `adms_robots`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sts_artigos`
--
ALTER TABLE `sts_artigos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sts_carousels`
--
ALTER TABLE `sts_carousels`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sts_cats_artigos`
--
ALTER TABLE `sts_cats_artigos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sts_contatos`
--
ALTER TABLE `sts_contatos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sts_paginas`
--
ALTER TABLE `sts_paginas`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sts_robots`
--
ALTER TABLE `sts_robots`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sts_seo`
--
ALTER TABLE `sts_seo`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sts_servicos`
--
ALTER TABLE `sts_servicos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sts_situacaos_pgs`
--
ALTER TABLE `sts_situacaos_pgs`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sts_sobres`
--
ALTER TABLE `sts_sobres`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sts_sobs_emps`
--
ALTER TABLE `sts_sobs_emps`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sts_tps_artigos`
--
ALTER TABLE `sts_tps_artigos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sts_tps_pgs`
--
ALTER TABLE `sts_tps_pgs`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sts_videos`
--
ALTER TABLE `sts_videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `adms_robots`
--
ALTER TABLE `adms_robots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `sts_artigos`
--
ALTER TABLE `sts_artigos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `sts_carousels`
--
ALTER TABLE `sts_carousels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `sts_cats_artigos`
--
ALTER TABLE `sts_cats_artigos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `sts_contatos`
--
ALTER TABLE `sts_contatos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de tabela `sts_paginas`
--
ALTER TABLE `sts_paginas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `sts_robots`
--
ALTER TABLE `sts_robots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `sts_seo`
--
ALTER TABLE `sts_seo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `sts_servicos`
--
ALTER TABLE `sts_servicos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `sts_situacaos_pgs`
--
ALTER TABLE `sts_situacaos_pgs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `sts_sobres`
--
ALTER TABLE `sts_sobres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `sts_sobs_emps`
--
ALTER TABLE `sts_sobs_emps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `sts_tps_artigos`
--
ALTER TABLE `sts_tps_artigos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `sts_tps_pgs`
--
ALTER TABLE `sts_tps_pgs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `sts_videos`
--
ALTER TABLE `sts_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
